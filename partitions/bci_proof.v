(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Correctness proofs
 This file contains the correctness proofs of all functions defined in bci.v. *)

Require Import bci.

Ltac unfold_BCI := repeat (unfold Pair || unfold T_bool || unfold F_bool || unfold Not
                           || unfold ElimB || unfold CopyB || unfold And || unfold Or
                           || unfold Elim_pairB || unfold Copy_pairB || unfold ITE).

(** ** Correctness of [Not] *)

Proposition Not_t: forall x y, Not @ T_bool @ x @ y = F_bool @ x @ y.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Not_f: forall x y, Not @ F_bool @ x @ y = T_bool @ x @ y.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

(** ** Correctness of [ElimB] *)

Proposition ElimB_t: ElimB @ T_bool = I.
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition ElimB_f: ElimB @ F_bool = I.
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

(** ** Correctness of [Or] *)

Proposition Or_tt: Or @ T_bool @ T_bool = T_bool.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Or_tf: forall x y, Or @ T_bool @ F_bool @ x @ y = T_bool @ x @ y.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Or_ft: forall x y, Or @ F_bool @ T_bool @ x @ y = T_bool @ x @ y.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Or_ff: forall x y, Or @ F_bool @ F_bool @ x @ y = F_bool @ x @ y.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

(** ** Correctness of [And] *)

Proposition And_tt: And @ T_bool @ T_bool = T_bool.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition And_tf: And @ T_bool @ F_bool = F_bool.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition And_ft: And @ F_bool @ T_bool = F_bool.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition And_ff: And @ F_bool @ F_bool = F_bool.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

(** ** Correctness of [ITE] *)

Proposition ITE_t: forall E x y, E @ x = I -> E @ y = I -> ITE @ T_bool @ x @ y @ E = x.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  rewrite H0.
  rewrite I_ax.
  reflexivity.
Qed.

Proposition ITE_f: forall E x y, E @ x = I -> E @ y = I -> ITE @ F_bool @ x @ y @ E = y.
Proof.
  intros.
  unfold_BCI.
  rewrite_BCI.
  rewrite H.
  rewrite I_ax.
  reflexivity.
Qed.

(** ** Correctness of [CopyB] *)

Proposition Copy_t: CopyB @ T_bool = Pair @ T_bool @ T_bool.
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Copy_f: CopyB @ F_bool = Pair @ F_bool @ F_bool.
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

(** ** Correctness of [Elim_pairB] *)

Proposition Elim_pairB_tt: Elim_pairB @ (Pair @ T_bool @ T_bool) = I.
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Elim_pairB_tf: Elim_pairB @ (Pair @ T_bool @ F_bool) = I.
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Elim_pairB_ft: Elim_pairB @ (Pair @ F_bool @ T_bool) = I.
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Elim_pairB_ff: Elim_pairB @ (Pair @ F_bool @ F_bool) = I.
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

(** ** Correctness of [Copy_pairB] *)

Proposition Copy_pairB_tt:
  Copy_pairB @ (Pair @ T_bool @ T_bool) = Pair @ (Pair @ T_bool @ T_bool) @ (Pair @ T_bool @ T_bool).
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Copy_pairB_tf:
  Copy_pairB @ (Pair @ T_bool @ F_bool) = Pair @ (Pair @ T_bool @ F_bool) @ (Pair @ T_bool @ F_bool).
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Copy_pairB_ft:
  Copy_pairB @ (Pair @ F_bool @ T_bool) = Pair @ (Pair @ F_bool @ T_bool) @ (Pair @ F_bool @ T_bool).
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.

Proposition Copy_pairB_ff:
  Copy_pairB @ (Pair @ F_bool @ F_bool) = Pair @ (Pair @ F_bool @ F_bool) @ (Pair @ F_bool @ F_bool).
Proof.
  unfold_BCI.
  rewrite_BCI.
  reflexivity.
Qed.
