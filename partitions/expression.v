(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Expressions with free variables and λ* *)
(** In order to implement λ*, this files provides a type for expressions containing free variables,
 as well as the implementation of λ* itself and the proof of its correctness. *)

Require Import List.
Import ListNotations.
Require Import Program.
Require Import Logic.FunctionalExtensionality.
Require Import bci.
Require Import partition.

(** ** Expressions *)

(** The type of variables *)

Parameter Var: Type.

(** The type of environments: lists of variables *)

Definition Env := list Var.

(** The type of terms built form applications of elements of a BCI algebra and free variables *)

Inductive Exp: Env -> Type :=
  | CAR: Carrier -> Exp []
  | VAR: forall v, Exp [v]
  | APP: forall {E E_fun E_arg}, Exp E_fun -> Exp E_arg -> E_fun <! E !> E_arg -> Exp E.

(** The three basic axioms of BCI algebras, for expressions *)

Axiom B_exp_ax: forall E1 E2 E12 E3 E23 E123 e1 e2 e3
                  (P1: [] <! E1 !> E1) (P2: E1 <! E12 !> E2) (P3: E12 <! E123 !> E3)
                  (P4: E2 <! E23 !> E3) (P5: E1 <! E123 !> E23),
    APP (APP (APP (CAR B) e1 P1) e2 P2) e3 P3 = APP e1 (APP e2 e3 P4) P5.

Axiom C_exp_ax: forall E1 E2 E12 E3 E13 E123 e1 e2 e3
                  (P1: [] <! E1 !> E1) (P2: E1 <! E12 !> E2) (P3: E12 <! E123 !> E3)
                  (P4: E1 <! E13 !> E3) (P5: E13 <! E123 !> E2),
    APP (APP (APP (CAR C) e1 P1) e2 P2) e3 P3 = APP (APP e1 e3 P4) e2 P5.

Axiom I_exp_ax: forall E e (P: [] <! E !> E),
    APP (CAR I) e P = e.

(** A substitution function, such that [substitute x _ _ M N] gives [M[N/x]] *)

Lemma substitute: forall {x: Var} {EM EM' EMN EN},
    [x] <! EM !> EM' -> EM' <! EMN !> EN ->
    Exp EM -> Exp EN -> Exp EMN.
Proof.
  intros ? ? ? ? ? pM pMN eM.
  revert x EM' EMN EN pM pMN.
  induction eM as [| |? ? ? ? ? ? ? pfunarg]; intros ? ? ? ? pM pMN eN.
  + inversion pM.
  + inversion pM as [|x0 l l1 l2 pnil |x0 l l1 l2 pnil]; inversion pnil; try symmetry in *; subst.
    apply Part_right_eq in pMN; subst.
    apply eN.
  + destruct (Part_square pfunarg pM) as [E_funx [E_argx [E_fun' [E_arg' [[[pfun parg] px] pM']]]]].
    inversion px as [|? lnil1 lnil2 E_argx0 pnil|? lnil1 E_funx0 lnil2 pnil]; try symmetry in *; subst.
    * inversion pnil; subst.
      apply Part_right_eq in parg; symmetry in parg; subst.
      apply Part_swap in pM'.
      destruct (Part_assoc pM' pMN) as [E_fun'N [pMN2 pfun'N]].
      eapply APP.
      ++ apply (IHeM1 _ _ _ _ pfun pfun'N eN).
      ++ apply eM2.
      ++ apply (Part_swap pMN2).
    * inversion pnil; subst.
      apply Part_right_eq in pfun; symmetry in pfun; subst.
      destruct (Part_assoc pM' pMN) as [E_arg'N [pMN2 parg'N]].
      eapply APP.
      ++ apply eM1.
      ++ apply (IHeM2 _ _ _ _ parg parg'N eN).
      ++ assumption.
Defined.

(** ** Implementing λ* *)
(** The [lambda_s] function, implementing λ* for expressions *)

Definition lambda_s: forall v {E E'}, [v] <! E !> E' -> Exp E -> Exp E'.
  intros v E E' pEv e.
  revert E' pEv.
  induction e as [| |? E_fun E_arg e_fun IHe_fun e_arg IHe_arg pfunarg]; intros E' pEv.
  + inversion pEv.
  + inversion pEv as [|x l l1 l2 pnil|x l l1 l2 pnil]; inversion pnil; subst.
    apply (CAR I).
  + destruct (Part_square pfunarg pEv) as [E_funv [E_argv [E_fun' [E_arg' [[[pfun parg] pv] pE']]]]].
    inversion pv as [|x l l1 l2 pnil|x l l1 l2 pnil]; inversion pnil; subst.
    * eapply APP.
      ++ eapply APP.
         ** apply (CAR C).
         ** apply IHe_fun.
            apply pfun.
         ** apply Part_right.
      ++ apply e_arg.
      ++ apply Part_right_eq in parg; symmetry in parg; subst.
         assumption.
    * eapply APP.
      ++ eapply APP.
         ** apply (CAR B).
         ** apply e_fun.
         ** apply Part_right.
      ++ apply IHe_arg.
         apply parg.
      ++ apply Part_right_eq in pfun as eqfun; symmetry in eqfun; subst.
         assumption.
Defined.

(** ** Correctness *)
(** Correctness of [lambda_s]: we must prove that λ*x.M·N = M#[#N/x#]# *)

Theorem lambda_s_correctness: forall v E E' E'' E''' (p1: [v] <! E !> E') (p2: E' <! E'' !> E''')
                                (e1: Exp E) (e2: Exp E'''),
    APP (lambda_s v p1 e1) e2 p2 = substitute p1 p2 e1 e2.
Proof.
  intros.
  induction e1.
  + inversion p1.
  + repeat dependent destruction p1.
    apply Part_right_eq in p2 as eq; subst.
    compute.
    rewrite I_exp_ax.
    admit.
  + admit.
Admitted.

(** ** Back to [Carrier] from [Exp []] *)
(** Getting back to a [Carrier] from an expression without free variables. *)

Program Fixpoint Exp_to_BCI (e: Exp []): Carrier :=
  match e with
  | CAR c => c
  | VAR _ => _
  | APP e1 e2 _ => (Exp_to_BCI e1) @ (Exp_to_BCI e2)
  end.

Next Obligation.
  inversion wildcard'2; subst.
  reflexivity.
Defined.

Next Obligation.
  inversion wildcard'2; subst.
  reflexivity.
Defined.
