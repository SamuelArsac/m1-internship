(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Partitions of a list into two lists *)

Require Import List.
Require Import Program.
Import ListNotations.

Section Partition.

  Context {A: Type}.

  (** ** Definition *)

  Inductive Partition: list A -> list A -> list A -> Type :=
  | part_nil: Partition [] [] []
  | part_skipl x {l lleft lright}: Partition l lleft lright -> Partition (x :: l) (x :: lleft) lright
  | part_skipr y {l lleft lright}: Partition l lleft lright -> Partition (y :: l) lleft (y :: lright).

  Notation "ll <! l !> lr" := (Partition l ll lr) (at level 30).

  (** ** Some basic lemmas *)

  (** For when one of the two sublists is empty *)

  Lemma Part_left: forall l, l <! l !> [].
  Proof.
    induction l.
    + apply part_nil.
    + apply part_skipl.
      assumption.
  Defined.

  Lemma Part_right: forall l, [] <! l !> l.
  Proof.
    induction l.
  + apply part_nil.
  + apply part_skipr.
    assumption.
  Defined.

  Lemma Part_right_eq: forall {l1 l2}, [] <! l1 !> l2 -> l1 = l2.
  Proof.
    induction l1; induction l2; try reflexivity; inversion 1; subst.
    apply IHl1 in X0.
    rewrite X0.
    reflexivity.
  Defined.

  Lemma Part_left_eq: forall {l1 l2}, l1 <! l2 !> [] -> l1 = l2.
  Proof.
    induction l1; induction l2; try reflexivity; inversion 1; subst.
    apply IHl1 in X0.
    rewrite X0.
    reflexivity.
  Defined.

  (** Swapping the two sublists *)

  Lemma Part_swap: forall {l l1 l2}, l1 <! l !> l2 -> l2 <! l !> l1.
  Proof.
    intros l l1 l2 Part.
    induction Part; subst.
    + apply part_nil.
    + apply part_skipr.
      assumption.
    + apply part_skipl.
      assumption.
  Defined.

  (** Combining two partitions of the same list *)

  Lemma Part_square: forall {l lleft lright ltop lbottom},
      lleft <! l !> lright -> ltop <! l !> lbottom
      -> {ltopleft &
         {ltopright &
          {lbottomleft &
           {lbottomright &
            ltopleft <! lleft !> lbottomleft
            * ltopright <! lright !> lbottomright
            * ltopleft <! ltop !> ltopright
            * lbottomleft <! lbottom !> lbottomright
        }}}}%type.
  Proof.
    intros l lleft lright ltop lbottom Part1.
    revert ltop lbottom.
    induction Part1; intros ltop lbottom Part2; inversion Part2; subst;
      try (apply IHPart1 in X; destruct X as
               [ltopleft [ltopright [lbottomleft [lbottomright [[[P1 P2] P3] P4]]]]]).
    + exists [].
      exists [].
      exists [].
      exists [].
      repeat split; apply part_nil.
    + exists (x :: ltopleft).
      exists ltopright.
      exists lbottomleft.
      exists lbottomright.
      repeat split; (try apply part_skipl); assumption.
    + exists ltopleft.
      exists ltopright.
      exists (x :: lbottomleft).
      exists lbottomright.
      repeat split; (try apply part_skipr); (try apply part_skipl); assumption.
    + exists ltopleft.
      exists (y :: ltopright).
      exists lbottomleft.
      exists lbottomright.
      repeat split; (try apply part_skipr); (try apply part_skipl); assumption.
    + exists ltopleft.
      exists ltopright.
      exists lbottomleft.
      exists (y :: lbottomright).
      repeat split; (try apply part_skipr); assumption.
  Defined.


(** Creating a new list from two sequential partitions:
from
<<
               ltop
                <!
                l
                !>
   lbotleft <! lbot !> lbotright
>>
we have a list [l'] such that
<<
                         ltop
                          !>
     lbotleft  <!  l  !>  l'
                          <!
                       lbotright
>>
 *)

  Lemma Part_assoc: forall {l ltop lbot lbotleft lbotright},
      lbotleft <! lbot !> lbotright
      -> lbot <! l !> ltop
      -> {l' & lbotleft <! l !> l' * lbotright <! l' !> ltop}%type.
  Proof.
    intros ? ? ? ? ? p1 p2.
    revert lbotleft lbotright p1.
    induction p2; intros.
    + inversion p1; subst.
      exists [].
      split; apply part_nil.
    + remember (x :: lleft) as xlleft in p1.
      revert lleft p2 IHp2 Heqxlleft.
      destruct p1; intros.
      * inversion Heqxlleft.
      * inversion Heqxlleft; subst.
        apply IHp2 in p1.
        destruct p1 as [l' [pl pr]].
        exists l'.
        split; auto using part_skipl.
      * inversion Heqxlleft; subst.
        apply IHp2 in p1.
        destruct p1 as [l' [pl pr]].
        exists (x :: l').
        split; auto using part_skipl, part_skipr.
    + apply IHp2 in p1.
      destruct p1 as [l' [pl pr]].
      exists (y :: l').
      split; auto using part_skipr.
  Defined.

End Partition.

Notation "ll <! l !> lr" := (Partition l ll lr) (at level 30).
