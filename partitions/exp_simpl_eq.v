(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Equality between the definitions of [bci] and [epx_def] *)

Require Import bci.
Require Import expression.
Require Import exp_def.

(** ** Pair *)

Lemma Pair_simpl: Exp_to_BCI Pair_exp = Pair.
Proof.
  auto.
Qed.

(** ** Booleans *)

Lemma T_bool_simpl: Exp_to_BCI T_bool_exp = T_bool.
Proof.
  auto.
Qed.

Lemma F_bool_simpl: Exp_to_BCI F_bool_exp = F_bool.
Proof.
  auto.
Qed.

(** ** Boolean operations *)

Lemma Not_simpl: Exp_to_BCI Not_exp = Not.
Proof.
  auto.
Qed.

Lemma ElimB_simpl: Exp_to_BCI ElimB_exp = ElimB.
Proof.
  auto.
Qed.

Lemma Or_simpl: Exp_to_BCI Or_exp = Or.
Proof.
  auto.
Qed.

Lemma And_simpl: Exp_to_BCI And_exp = And.
Proof.
  auto.
Qed.

Lemma ITE_simpl: Exp_to_BCI ITE_exp = ITE.
Proof.
  auto.
Qed.

(** ** Duplication and deletion for pairs *)

Lemma CopyB_simpl: Exp_to_BCI CopyB_exp = CopyB.
Proof.
  simpl.
  rewrite_BCI.
  reflexivity.
Qed.

Lemma Elim_pairB_simpl: Exp_to_BCI (Elim_pair_exp ElimB_exp ElimB_exp) = Elim_pairB.
Proof.
  auto.
Qed.

Lemma Copy_pairB_simpl: Exp_to_BCI (Copy_pair_exp CopyB_exp CopyB_exp) = Copy_pairB.
Proof.
  simpl.
  rewrite_BCI.
  unfold Copy_pairB.
  reflexivity.
Qed.
