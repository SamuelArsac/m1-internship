(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * BCI
 BCI algebras are defined as a record type, with a carrier set,
the application law and the B, C and I combinator with their properties. *)

(** ** Definition *)

Reserved Infix "@" (at level 20, left associativity).
Record BCI :=
  {Carrier_set: Type;
   App: Carrier_set -> Carrier_set -> Carrier_set where "A @ B" := (App A B) ;
   B_combinator: Carrier_set;
   C_combinator: Carrier_set;
   I_combinator: Carrier_set;
   B_ax: forall x y z: Carrier_set, B_combinator @ x @ y @ z = x @ (y @ z);
   C_ax: forall x y z: Carrier_set, C_combinator @ x @ y @ z = x @ z @ y;
   I_ax: forall x: Carrier_set, I_combinator @ x = x;
  }.

(** ** Notations *)

Parameter bci: BCI.

Infix "@" := (App bci).
Notation "'Carrier'" := (Carrier_set bci).
Notation "'B'" := (B_combinator bci).
Notation "'C'" := (C_combinator bci).
Notation "'I'" := (I_combinator bci).

(** Rewriting tactic for [Carrier] objects *)

Ltac rewrite_BCI := repeat (rewrite (C_ax bci) || rewrite (B_ax bci) || rewrite (I_ax bci)).

(** * Some specific [Carrier] objects *)

(** ** Pair
 [Pair] is defined to behave as pairs in lambda-calculus: [Pair @ x @ y @ z = z @ x @ y]. *)

Definition Pair := C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I.

(** ** Booleans *)

(** True is defined to behave like [Pair]: [T_bool @ x @ y = Pair @ x @ y] *)

Definition T_bool := Pair.

(** False is defined to behave like [Pair] but swaps the arguments: [F_bool @ x @ y = Pair @ y @ x] *)

Definition F_bool := B @ (C @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I.

(** ** Boolean functions *)

Definition Not := C @ (B @ B @ (B @ C @ (C @ (B @ B @ I) @ I))) @ I.

(** Deletion for booleans: [ElimB @ T/F_bool = I] *)

Definition ElimB := C @ (C @ (C @ I @ I) @ I) @ I.

Definition Or := C @ (B @ C @ (C @ (B @ C @ (C @ (B @ B @ I) @ I))
                                 @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I)))
                   @ (C @ (C @ (C @ I @ I) @ I) @ I).

Definition And := C @ (B @ C @ (C @ (B @ B @ (C @ I @ (B @ (C @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I)))
                                                                        @ I) @ I)) @ I))) @ I))
                    @ (C @ (C @ (C @ I @ I) @ I) @ I).

(** If-Then-Else: if [E] is an eliminator for [x] and [y], [ITE @ x @ y @ z @ E = If x Then y Else z] *)

Definition ITE := C @ (B @ C @ (B @ (B @ C) @ (B @ (B @ (B @ B))
                                                 @ (C @ (B @ B @ (B @ C @ (C @ (B @ B @ I) @ I))) @ I))))
                    @ I.

(** Duplication for booleans: [CopyB @ T/F_bool = Pair @ T/F_bool @ T/F_bool] *)

Definition CopyB :=  C @
  (C @
   (C @ I @
      (C @ (C @ I @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I)) @ (C @ (B @ B @
        (B @ C @ (B @ (C @ I) @ I))) @ I))) @
   (C @ (C @ I @ (B @ (C @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I)) @
    (B @ (C @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I))) @
  (C @ (B @ B @ I) @
   (C @ (B @ B @ (B @ B @ I)) @
    (C @
     (B @ B @
      (B @ C @
       (B @ (B @ B) @
        (B @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I)) @
         (B @ (C @ (B @ (C @ (C @ (C @ I @ I) @ I) @ I) @ I)) @ I))))) @
     (B @ (C @ (B @ (C @ (C @ (C @ I @ I) @ I) @ I) @ I)) @ I)))).

(** ** Deletion and duplication for pairs of booleans
 Behave like the previous [Elim] and [Copy]. *)

Definition Elim_pairB := C @ I @ (C @ (B @ B @ (B @ (C @ (C @ (C @ I @ I) @ I) @ I) @ I))
                                        @ (B @ (C @ (C @ (C @ I @ I) @ I) @ I) @ I)).


Definition Copy_pairB := C @ I @
  (C @
   (B @ B @
    (B @
     (C @
      (C @
       (C @ I @
        (C @ (C @ I @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I)) @
         (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I))) @
       (C @ (C @ I @ (B @ (C @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I)) @
        (B @ (C @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I))) @
      (C @ (B @ B @ I) @
       (C @ (B @ B @ (B @ B @ I)) @
        (C @
         (B @ B @
          (B @ C @
           (B @ (B @ B) @
            (B @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I)) @
             (B @ (C @ (B @ (C @ (C @ (C @ I @ I) @ I) @ I) @ I)) @ I))))) @
         (B @ (C @ (B @ (C @ (C @ (C @ I @ I) @ I) @ I) @ I)) @ I))))) @ I)) @
   (C @
    (B @ B @
     (B @ B @
      (B @
       (C @
        (C @
         (C @ I @
          (C @ (C @ I @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I)) @
           (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I))) @
         (C @ (C @ I @ (B @ (C @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I)) @
          (B @ (C @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I))) @
        (C @ (B @ B @ I) @
         (C @ (B @ B @ (B @ B @ I)) @
          (C @
           (B @ B @
            (B @ C @
             (B @ (B @ B) @
              (B @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I)) @
               (B @ (C @ (B @ (C @ (C @ (C @ I @ I) @ I) @ I) @ I)) @ I))))) @
           (B @ (C @ (B @ (C @ (C @ (C @ I @ I) @ I) @ I) @ I)) @ I))))) @ I))) @
    (C @
     (B @ B @
      (B @ C @
       (B @ (B @ B) @
        (B @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I)) @
         (C @ (B @ B @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I))))) @
     (C @ (B @ B @ (B @ (C @ (B @ B @ (B @ C @ (B @ (C @ I) @ I))) @ I) @ I)) @ I)))).
