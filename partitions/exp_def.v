(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Defintions
 This files contains the definitions of expressions without free variables [Exp []]
 from which the [Carrier] objects in [bci] are derived. The proofs of equality between
 these expressions and the definitions in [bci] are in [exp_simpl_eq]. *)

Require Import List.
Import ListNotations.
Require Import bci.
Require Import partition.
Require Import expression.

Parameter P Q x y z D: Var.

(** ** Pairs
 Pair := lam* x y z. z x y *)

Definition Pair_exp: Exp [].
  eapply (lambda_s x).
  + apply part_skipl.
    apply part_nil.
  + eapply (lambda_s y).
    * apply part_skipl.
      apply part_skipr.
      apply part_nil.
    * eapply (lambda_s z).
      ++ apply part_skipl.
         apply part_skipr.
         apply part_skipr.
         apply part_nil.
      ++ eapply APP.
         ** eapply APP.
            +++ apply (VAR z).
            +++ apply (VAR x).
            +++ apply part_skipl.
                apply part_skipr.
                apply part_nil.
         ** apply (VAR y).
         ** apply part_skipl.
            apply part_skipr.
            apply part_skipl.
            apply part_nil.
Defined.

(** ** Booleans *)

(** T_bool := Pair *)

Definition T_bool_exp := Pair_exp.

(** F_bool := lam* x y. Pair y x *)

Definition F_bool_exp: Exp [].
  eapply (lambda_s x).
  + apply Part_left.
  + eapply (lambda_s y).
    * apply part_skipr.
      apply Part_left.
    * eapply APP.
      ++ eapply APP.
         ** apply Pair_exp.
         ** apply (VAR y).
         ** apply Part_right.
      ++ apply (VAR x).
      ++ apply part_skipr.
         apply Part_left.
Defined.

(** *** Boolean operations *)

(** Not := lam* P x y. P y x *)

Definition Not_exp: Exp [].
  eapply (lambda_s P).
  + apply Part_left.
  + eapply (lambda_s x).
    * apply part_skipr.
      apply Part_left.
    * eapply (lambda_s y).
      ++ apply part_skipr.
         apply part_skipr.
         apply Part_left.
      ++ eapply APP.
         ** eapply APP.
            +++ apply (VAR P).
            +++ apply (VAR y).
            +++ apply part_skipl.
                apply Part_right.
         ** apply (VAR x).
         ** apply part_skipl.
            apply part_skipr.
            apply Part_left.
Defined.

(** ElimB := lam* x. x @ I @ I @ I *)

Definition ElimB_exp: Exp [].
  eapply (lambda_s x).
  + apply Part_left.
  + eapply APP.
    * eapply APP.
      ++ eapply APP.
         ** apply (VAR x).
         ** apply (CAR I).
         ** apply Part_left.
      ++ apply (CAR I).
      ++ apply Part_left.
    * apply (CAR I).
    * apply Part_left.
Defined.

(** Or := lam* P Q. P @ Q @ T_bool @ ElimB *)

Definition Or_exp: Exp [].
  eapply (lambda_s P).
  + apply Part_left.
  + eapply (lambda_s Q).
    * apply part_skipr.
      apply Part_left.
    * eapply APP.
      ++ eapply APP.
         ** eapply APP.
            +++ apply (VAR P).
            +++ apply (VAR Q).
            +++ apply part_skipl.
                apply Part_right.
         ** apply T_bool_exp.
         ** apply Part_left.
      ++ apply ElimB_exp.
      ++ apply Part_left.
Defined.

(** And := lam* P Q. P @ F_bool @ Q @ ElimB *)

Definition And_exp: Exp [].
  eapply (lambda_s P).
  + apply Part_left.
  + eapply (lambda_s Q).
    * apply part_skipr.
      apply Part_left.
    * eapply APP.
      ++ eapply APP.
         ** eapply APP.
            +++ apply (VAR P).
            +++ apply F_bool_exp.
            +++ apply Part_left.
         ** apply (VAR Q).
         ** apply part_skipl.
            apply Part_right.
      ++ apply ElimB_exp.
      ++ apply Part_left.
Defined.

(** *** If-then-else *)

(** ITE := lam* P x y D. P @ y @ x @ D
 with D @ x = I and D @ y = I *)

Definition ITE_exp: Exp [].
  eapply (lambda_s P).
  + apply Part_left.
  + eapply (lambda_s x).
    * apply part_skipr.
      apply Part_left.
    * eapply (lambda_s y).
      ++ repeat apply part_skipr.
         apply Part_left.
      ++ eapply (lambda_s D).
         ** repeat apply part_skipr.
            apply Part_left.
         ** eapply APP.
            +++ eapply APP.
                *** eapply APP.
                    ++++ apply (VAR P).
                    ++++ apply (VAR y).
                    ++++ apply part_skipl.
                         apply Part_right.
                *** apply (VAR x).
                *** apply part_skipl.
                    apply part_skipr.
                    apply Part_left.
            +++ apply (VAR D).
            +++ repeat apply part_skipl.
                apply Part_right.
Defined.

(** *** Duplication for booleans *)
(** CopyB := lam* P. P @ (Pair @ T_bool @ T_bool) @ (Pair @ F_bool @ F_bool)
                       @ (lam* U V. U @ (lam* u1 u2.
                                          V @ (lam* v1 v2.
                                                (Pair @ (id @ v1 @ u1) @ (id @ v2 @ u2))))) *)

Parameter U V u1 u2 v1 v2: Var.

Definition CopyB_exp: Exp [].
  eapply (lambda_s P).
  + apply Part_left.
  + eapply APP.
    * eapply APP.
      ++ eapply APP.
         ** apply (VAR P).
         ** eapply APP.
            +++ eapply APP.
                *** apply Pair_exp.
                *** apply T_bool_exp.
                *** apply part_nil.
            +++ apply T_bool_exp.
            +++ apply part_nil.
         ** apply Part_left.
      ++ eapply APP.
         ** eapply APP.
            +++ apply Pair_exp.
            +++ apply F_bool_exp.
            +++ apply part_nil.
         ** apply F_bool_exp.
         ** apply part_nil.
      ++ apply Part_left.
    * eapply (lambda_s U).
      ++ apply Part_left.
      ++ eapply (lambda_s V).
         ** apply part_skipr.
            apply Part_left.
         ** eapply APP.
            +++ apply (VAR U).
            +++ eapply (lambda_s u1).
                *** apply part_skipr.
                    apply Part_left.
                *** eapply (lambda_s u2).
                    ++++ repeat apply part_skipr.
                         apply Part_left.
                    ++++ eapply APP.
                         **** apply (VAR V).
                         **** eapply (lambda_s v1).
                              +++++ apply part_skipr.
                              apply part_skipl.
                              apply Part_right.
                              +++++ eapply (lambda_s v2).
                              ***** do 3 apply part_skipr.
                              apply Part_left.
                              ***** eapply APP.
                              ++++++ eapply APP.
                              ****** apply Pair_exp.
                              ****** eapply APP.
                              +++++++ eapply APP.
                              ******* apply ElimB_exp.
                              ******* apply (VAR v1).
                              ******* apply Part_right.
                              +++++++ apply (VAR u1).
                              +++++++ apply part_skipr.
                              apply Part_left.
                              ****** apply Part_right.
                              ++++++ eapply APP.
                              ****** eapply APP.
                              +++++++ apply ElimB_exp.
                              +++++++ apply (VAR v2).
                              +++++++ apply Part_right.
                              ****** apply (VAR u2).
                              ****** apply part_skipr.
                              apply Part_left.
                              ++++++ do 2 apply part_skipl.
                              apply Part_right.
                         **** apply part_skipl.
                              apply Part_right.
            +++ apply part_skipl.
                apply Part_right.
    * apply Part_left.
Defined.

(** *** Deletion for pairs *)
(** lam* P. P @ (lam* x y. (x_elim @ x) @ (y_elim @ y)) *)

Definition Elim_pair_exp (x_elim y_elim: Exp []): Exp [].
  eapply (lambda_s P).
  + apply Part_left.
  + eapply APP.
    * apply (VAR P).
    * eapply (lambda_s x).
      ++ apply Part_left.
      ++ eapply (lambda_s y).
         ** apply part_skipr.
            apply Part_left.
         ** eapply APP.
            +++ eapply APP.
                *** apply x_elim.
                *** apply (VAR x).
                *** apply Part_right.
            +++ eapply APP.
                *** apply y_elim.
                *** apply (VAR y).
                *** apply Part_right.
            +++ apply part_skipl.
                apply Part_right.
    * apply Part_left.
Defined.

(** **** Correctness of [Elim_pair] *)
(** Both this function and the next one are proven her since they depend on some variables *)

Lemma Elim_pair_correctness: forall (x_elim y_elim: Exp []) (x y x_elim_c y_elim_c: Carrier),
    Exp_to_BCI x_elim = x_elim_c -> Exp_to_BCI y_elim = y_elim_c ->
    x_elim_c @ x = I -> y_elim_c @ y = I ->
    Exp_to_BCI (Elim_pair_exp x_elim y_elim) @ (Pair @ x @ y) = I.
Proof.
  intros ? ? x y ? ? BCI_x_elim BCI_y_elim ax_elim_x ax_elim_y.
  simpl.
  rewrite BCI_x_elim.
  rewrite BCI_y_elim.
  unfold Pair.
  rewrite_BCI.
  rewrite ax_elim_x.
  rewrite ax_elim_y.
  rewrite_BCI.
  reflexivity.
Qed.

(** *** Duplication for pairs *)
(** lam* P. P @ (lam* x y. (x_copy @ x) @
                           (lam* x1 x2. (y_copy @ y) @
                                         (lam* y1 y2. Pair @ (Pair @ x1 @ y1)
                                                           @ (Pair @ x2 @ y2)))) *)

Parameter x1 x2 y1 y2: Var.

Definition Copy_pair_exp (x_copy y_copy: Exp []): Exp [].
  eapply (lambda_s P).
  + apply Part_left.
  + eapply APP.
    * apply (VAR P).
    * eapply (lambda_s x).
      ++ apply Part_left.
      ++ eapply (lambda_s y).
         ** apply part_skipr.
            apply Part_left.
         ** eapply APP.
            +++ eapply APP.
                *** apply x_copy.
                *** apply (VAR x).
                *** apply Part_right.
            +++ eapply (lambda_s x1).
                *** apply part_skipr.
                    apply Part_left.
                *** eapply (lambda_s x2).
                    ++++ do 2 apply part_skipr.
                         apply Part_left.
                    ++++ eapply APP.
                         **** eapply APP.
                              +++++ apply y_copy.
                              +++++ apply (VAR y).
                              +++++ apply Part_right.
                         **** eapply (lambda_s y1).
                              +++++ do 2 apply part_skipr.
                              apply Part_left.
                              +++++ eapply (lambda_s y2).
                              ***** do 3 apply part_skipr.
                              apply Part_left.
                              ***** eapply APP.
                              ++++++ eapply APP.
                              ****** apply Pair_exp.
                              ****** eapply APP.
                              +++++++ eapply APP.
                              ******* apply Pair_exp.
                              ******* apply (VAR x1).
                              ******* apply Part_right.
                              +++++++ apply (VAR y1).
                              +++++++ apply part_skipl.
                              apply Part_right.
                              ****** apply Part_right.
                              ++++++ eapply APP.
                              ****** eapply APP.
                              +++++++ apply Pair_exp.
                              +++++++ apply (VAR x2).
                              +++++++ apply Part_right.
                              ****** apply (VAR y2).
                              ****** apply part_skipl.
                              apply Part_right.
                              ++++++ do 2 (apply part_skipl; apply part_skipr).
                              apply part_nil.
                         **** apply part_skipl.
                              apply Part_right.
            +++ apply part_skipl.
                apply Part_right.
    * apply Part_left.
Defined.

(** **** Correctness of Copy_pair *)

Lemma Copy_pair_correctness: forall (x_copy y_copy: Exp []) (x y x_copy_c y_copy_c: Carrier),
    Exp_to_BCI x_copy = x_copy_c -> Exp_to_BCI y_copy = y_copy_c ->
    x_copy_c @ x = Pair @ x @ x -> y_copy_c @ y = Pair @ y @ y ->
    Exp_to_BCI (Copy_pair_exp x_copy y_copy) @ (Pair @ x @ y) = Pair @ (Pair @ x @ y) @ (Pair @ x @ y).
Proof.
  intros ? ? x y ? ? BCI_x_copy BCI_y_copy ax_copy_x ax_copy_y.
  simpl.
  rewrite BCI_x_copy.
  rewrite BCI_y_copy.
  unfold Pair.
  rewrite_BCI.
  rewrite ax_copy_x.
  rewrite ax_copy_y.
  unfold Pair.
  rewrite_BCI.
  reflexivity.
Qed.
