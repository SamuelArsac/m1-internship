partition:
	cd partitions\
	&& coqc bci.v\
	&& coqc bci_proof.v\
	&& coqc partition.v\
	&& coqc expression.v\
	&& coqc exp_def.v\
	&& coqc exp_simpl_eq.v

permutation:
	cd permutations\
	&& coqc bci.v\
	&& coqc permutation.v\
	&& coqc expression.v\

clean:
	rm -rf */.*.aux */*.glob */*.vo */*.vok */*.vos

partdoc:
	rm partitions/doc/* && coqdoc partitions/*.v -html -g -utf8 -toc -d partitions/doc

permdoc:
	rm permutations/doc/* && coqdoc permutations/*.v -html -g -utf8 -toc -d permutations/doc
