(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Permutations *)
(** This file contains an implementation of permutations
following the encoding of the standard library but using [Type] instead of [Prop]. *)

Require Import List.
Require Import RelationClasses.
Require Import Program.Equality.
Import ListNotations.


(** Rewritten two lemmas from the standard library with [Defined] instead of [Qed]. *)

Section Defined_app_lemmas.

  Context {A: Type}.

  Lemma app_assoc_def: forall (l1 l2 l3: list A), l1 ++ l2 ++ l3 = (l1 ++ l2) ++ l3.
  Proof.
    intros.
    induction l1.
    + reflexivity.
    + simpl.
      rewrite IHl1.
      reflexivity.
  Defined.

  Lemma app_nil_r_def: forall (l: list A), l ++ [] = l.
  Proof.
    intros.
    induction l; auto.
    simpl.
    rewrite IHl.
    reflexivity.
  Defined.

End Defined_app_lemmas.


(** Redefined [Add] from the standard library [List], with [Type] instead of [Prop]. *)

Section Addt.

  Context {A: Type}.

  Inductive Addt (a: A): list A -> list A -> Type :=
  | Addt_head l: Addt a l (a :: l)
  | Addt_cons x l l': Addt a l l' -> Addt a (x :: l) (x :: l').

  Lemma Addt_app: forall (a: A) l1 l2, Addt a (l1 ++ l2) (l1 ++ a :: l2).
  Proof.
    intros.
    induction l1; simpl.
    + apply Addt_head.
    + apply Addt_cons.
      assumption.
  Defined.

End Addt.


(** [Contains a l] is inhabited by the object pointing to the position of [a] in [l],
provided such an object exists. *)

Section Contains.

  Context {A: Type}.

  Inductive Contains (a: A): list A -> Type :=
  | Here : forall l, Contains a (a :: l)
  | Next : forall l a', Contains a l -> Contains a (a' :: l).

  Lemma Contains_app_head: forall {x l1} l2, Contains x l1 -> Contains x (l1 ++ l2).
  Proof.
    intros l1 l2 x Ct.
    induction Ct.
    + apply Here.
    + apply Next.
      apply IHCt.
  Defined.

  Lemma Contains_app_tail: forall l1 {l2 x}, Contains x l2 -> Contains x (l1 ++ l2).
  Proof.
    intros l1 l2 x.
    revert l2.
    induction l1; intros l2 Ct; auto.
    apply Next.
    apply IHl1.
    assumption.
  Defined.

  Lemma Contains_split: forall {l x}, Contains x l -> {l1 & {l2 | l = l1 ++ x :: l2}}.
  Proof.
    intros l x Ct.
    induction Ct as [|l y Ct].
    + exists [].
      exists l.
      reflexivity.
    + destruct IHCt as [l3 [l4 eq]].
      exists (y :: l3).
      exists l4.
      rewrite eq.
      reflexivity.
  Defined.

  Lemma Contains_delete: forall l1 l2 x {y},
      (Contains y (l1 ++ x :: l2) -> Contains y (l1 ++ l2) + (x = y))%type.
  Proof.
    intros l1 l2 x y Ct.
    induction l1 as [|z].
    + inversion Ct; subst.
      * right.
        reflexivity.
      * left.
        assumption.
    + inversion Ct as [|? ? Ct']; subst.
      * left.
        apply Here.
      * destruct IHl1 as [Ct''|]; auto.
        left.
        apply Next.
        assumption.
  Defined.

  Lemma Addt_contains {a l l'}: Addt a l l'
                                -> forall x, (Contains x l' -> Contains x (a :: l))
                                       * (Contains x (a :: l) -> Contains x l').
    intros.
    split; revert x; induction X; auto; intros.
    + remember (x :: l') as l'' in X0.
      destruct X0; inversion Heql''; subst.
      * apply Next.
        apply Here.
      * apply IHX in X0.
        inversion X0.
        ++ apply Here.
        ++ do 2 apply Next.
           assumption.
    + remember (a :: x :: l) as l'' in X0.
      destruct X0; inversion Heql''; subst.
      * apply Next.
        apply (IHX a).
        apply Here.
      * inversion X0.
        ++ apply Here.
        ++ apply Next.
           apply (IHX x0).
           apply Next.
           assumption.
  Defined.

  Lemma Addt_inv {a: A} {l}: Contains a l -> {l' & Addt a l' l}.
  Proof.
    intros.
    induction X.
    + exists l.
      apply Addt_head.
    + destruct IHX.
      exists (a' :: x).
      apply Addt_cons.
      assumption.
  Defined.

End Contains.

(** ** Lemmas for permutations *)
(** Rewritten parts of the standard library [Permutation] with permutations as a [Type] instead of a [Prop]. *)

Section Permutation.

  Context {A: Type}.

  Inductive Permutation : list A -> list A -> Type :=
  | perm_nil: Permutation [] []
  | perm_skip x {l l'} : Permutation l l' -> Permutation (x :: l) (x :: l')
  | perm_swap x y l : Permutation (y :: x :: l) (x :: y :: l)
  | perm_trans {l l' l''} :
      Permutation l l' -> Permutation l' l'' -> Permutation l l''.

  Notation "l1 >< l2" := (Permutation l1 l2) (at level 70).

  (** The following lemmas should be named and ordered as in the standard library *)

  Lemma Permutation_nil: forall l, [] >< l -> l = [].
  Proof.
    intros l p.
    remember [] as empty in p.
    induction p; auto; inversion Heqempty.
  Defined.

  Lemma Permutation_refl: forall l, l >< l.
  Proof.
    intro.
    induction l as [|x].
    + apply perm_nil.
    + apply perm_skip.
      assumption.
  Defined.

  Lemma Permutation_sym: forall {l l'}, l >< l' -> l' >< l.
  Proof.
    intros l l' p.
    induction p; eauto using perm_nil, perm_skip, perm_swap, perm_trans.
  Defined.

  Lemma Permutation_contains: forall {l l': list A} {a}, l >< l' -> Contains a l -> Contains a l'.
  Proof.
    intros.
    induction X.
    + assumption.
    + inversion X0.
      * subst.
        apply Here.
      * subst.
        apply Next.
        auto.
    + inversion X0.
      * apply Next.
        apply Here.
      * subst.
        inversion X.
        ++ apply Here.
        ++ subst.
           apply Next.
           apply Next.
           assumption.
    + auto.
  Defined.

  Lemma Permutation_app_tail: forall {l1 l1'} l2, l1 >< l1' -> l1 ++ l2 >< l1' ++ l2.
  Proof.
    intros l1 l1' l2 p.
    induction p; eauto using Permutation_refl, perm_trans, perm_skip, perm_swap.
  Defined.

  Lemma Permutation_app_head: forall l1 {l2 l2'}, l2 >< l2' -> l1 ++ l2 >< l1 ++ l2'.
  Proof.
    intros l1 l2 l2' p.
    induction l1 as [|x]; auto using Permutation_refl.
    simpl.
    apply perm_skip.
    assumption.
  Defined.

  Lemma Permutation_cons_append: forall l x, x :: l >< l ++ [x].
  Proof.
    intros.
    induction l as [|y].
    + apply Permutation_refl.
    + eapply perm_trans.
      * apply perm_swap.
      * apply perm_skip.
        assumption.
  Defined.

  Lemma Permutation_app_comm: forall l1 l2, l1 ++ l2 >< l2 ++ l1.
  Proof.
    intros.
    induction l1 as [|x].
    + simpl.
      rewrite app_nil_r_def.
      apply Permutation_refl.
    + pose (p := Permutation_cons_append l2 x).
      apply (perm_skip x) in IHl1.
      apply (Permutation_app_tail l1) in p.
      pose (p' := perm_trans IHl1 p).
      rewrite<- app_assoc_def in p'.
      assumption.
  Defined.

  Lemma Permutation_Addt: forall {a l l'}, Addt a l l' -> (a :: l) >< l'.
  Proof.
    intros.
    induction X.
    + apply Permutation_refl.
    + eapply perm_trans.
      * apply perm_swap.
      * apply perm_skip.
        assumption.
  Defined.

  Lemma Permutation_middle: forall l1 l2 x, (x :: l1 ++ l2) >< (l1 ++ x :: l2).
  Proof.
    intros.
    induction l1 as [|y].
    + apply Permutation_refl.
    + assert (p: x :: y :: l1 ++ l2 >< y :: x :: l1 ++ l2).
      * apply perm_swap.
      * apply (perm_skip y) in IHl1.
        apply (perm_trans p IHl1).
  Defined.

  Lemma Permutation_elt: forall {l1 l2 l3 l4} x, l1 ++ l2 >< l3 ++ l4 -> l1 ++ x :: l2 >< l3 ++ x :: l4.
  Proof.
    intros l1 l2 l3 l4 x p.
    apply (perm_skip x) in p.
    assert (p': l1 ++ x :: l2 >< x :: l1 ++ l2).
    + apply Permutation_sym.
      apply Permutation_middle.
    + assert (p'': x :: l3 ++ l4 >< l3 ++ x :: l4).
      * apply Permutation_middle.
      * apply (perm_trans p) in p''.
        apply (perm_trans p' p'').
  Defined.

  Theorem Permutation_ind_bis:
    forall P: list A -> list A -> Type,
      P [] [] ->
      (forall x l l', l >< l' -> P l l' -> P (x :: l) (x :: l')) ->
      (forall x y l l', l >< l' -> P l l' -> P (y :: x :: l) (x :: y :: l')) ->
      (forall l l' l'', l >< l' -> P l l' -> l' >< l'' -> P l' l'' -> P l l'') ->
      forall l l', l >< l' -> P l l'.
  Proof.
    intros P Hnil Hskip Hswap Htrans l l' perm.
    assert (P_refl: forall l, P l l).
    + intro.
      induction l0.
      * assumption.
      * apply Hskip.
        ++ apply Permutation_refl.
        ++ assumption.
    + induction perm.
      * assumption.
      * auto.
      * apply Htrans with (l' := x :: y :: l); auto.
        ++ apply perm_swap.
        ++ apply Hswap.
           ** apply Permutation_refl.
           ** apply P_refl.
        ++ apply Permutation_refl.
      * apply Htrans with (l' := l'); auto.
  Defined.

  Lemma Permutation_Addt_inv: forall {a: A} {l1 l2},
      l1 >< l2 -> forall l1' l2', Addt a l1' l1 -> Addt a l2' l2 -> l1' >< l2'.
  Proof.
    intro a.
    refine (Permutation_ind_bis _ _ _ _ _).
    + intros l'1 l'2 Add_l'1 Add_l'2.
      inversion Add_l'1.
    + intros x l l' perm IH l1' l2' Add_l1' Add_l2'.
      inversion Add_l1'; inversion Add_l2'.
        * assumption.
        * subst.
          apply Permutation_Addt in X.
          apply Permutation_sym in X.
          apply (perm_trans perm X).
        * subst.
          apply Permutation_Addt in X.
          apply (perm_trans X perm).
        * apply perm_skip.
          apply IH; auto.
    + intros x y l l' perm IH l1' l2' Add1 Add2.
      inversion Add1; inversion Add2; subst.
      * apply (perm_skip x) in perm.
        assumption.
      * inversion X.
        ++ apply (perm_skip x) in perm.
           assumption.
        ++ subst.
           apply perm_skip.
           apply Permutation_Addt in X0.
           apply Permutation_sym in X0.
           eapply (perm_trans perm X0).
      * inversion X.
        ++ apply perm_skip.
           assumption.
        ++ subst.
           apply perm_skip.
           apply Permutation_Addt in X0.
           apply (perm_trans X0 perm).
      * inversion X; inversion X0; subst.
        ++ apply perm_skip.
           assumption.
        ++ apply Permutation_Addt in X1.
           apply Permutation_sym in X1.
           apply (perm_trans perm) in X1.
           apply (perm_skip y) in X1.
           apply (perm_trans X1 (perm_swap x y l3)).
        ++ apply Permutation_Addt in X.
           apply (perm_skip x) in perm.
           apply (perm_trans X perm).
        ++ apply (perm_trans (perm_skip y (perm_skip x (IH _ _ X1 X2))) (perm_swap x y l3)).
    + intros l1 l l2 p1 IH1 p2 IH2 l1' l2' Add1 Add2.
      assert (Ct: Contains a l).
      * apply (Permutation_contains p1).
        apply (snd (Addt_contains Add1 a)).
        apply Here.
      * destruct (Addt_inv Ct) as [l' Add3].
        apply (perm_trans (l' := l')); auto.
  Defined.

  Lemma Permutation_app_inv: forall {x: A} {l1 l2 l3 l4}, l1 ++ x :: l2 >< l3 ++ x :: l4
                                                    -> l1 ++ l2 >< l3 ++ l4.
  Proof.
    intros.
    apply (Permutation_Addt_inv (l1 := l1 ++ x :: l2) (l2 := l3 ++ x :: l4) (a := x));
    auto using Addt_head, Addt_app.
  Defined.

  Lemma Permutation_cons_inv: forall {x l l'}, x :: l >< x :: l' -> l >< l'.
  Proof.
    intros.
    apply (Permutation_app_inv (l1 := []) (l3 := []) X).
  Defined.

  Lemma Permutation_length_1_inv: forall {l x}, [x] >< l -> l = [x].
  Proof.
    intros l x p.
    remember [x] as singleton in p.
    induction p as [|y l l' p| |]; auto; inversion Heqsingleton.
    subst.
    apply Permutation_nil in p as eq_nil.
    rewrite eq_nil.
    reflexivity.
  Defined.

  Lemma Permutation_vs_elt_inv: forall {a: A} {l1 l2 l3},
      l1 >< l2 ++ a :: l3 -> {l4 & {l5 | l1 = l4 ++ a :: l5}}.
  Proof.
    intros.
    dependent induction X.
    + apply app_cons_not_nil in x.
      contradiction.
    + destruct l2; inversion x; subst.
      * exists [].
        exists l.
        reflexivity.
      * destruct (IHX l3 l2 a); auto.
        ++ destruct s.
           exists (a0 :: x0).
           exists x1.
           rewrite e.
           reflexivity.
    + destruct l2; inversion x; subst.
      * exists [y].
        exists l.
        reflexivity.
      * destruct l2; inversion H1; subst.
        ++ exists [].
           exists (a0 :: l3).
           reflexivity.
        ++ exists (a1 :: a0 :: l2).
           exists l3.
           reflexivity.
    + destruct (IHX2 l3 l2 a); auto.
      destruct s.
      apply IHX1 with (l2 := x) (l3 := x0).
      assumption.
  Defined.

  Lemma Permutation_app_nil: forall l, l >< l ++ [].
  Proof.
    induction l as [|x].
    + apply perm_nil.
    + simpl.
      apply perm_skip.
      assumption.
  Defined.

  Lemma Permutation_sym_refl: forall l, Permutation_sym (Permutation_refl l) = Permutation_refl l.
  Proof.
    intro.
    induction l as [|x]; auto.
    simpl.
    rewrite IHl.
    reflexivity.
  Defined.

  Lemma Permutation_sym_double: forall {l1 l2} (p: l1 >< l2), Permutation_sym (Permutation_sym p) = p.
  Proof.
    intros.
    induction p as [ |x l1 l2 p| |l1 l2 l3 p1 ? p2]; auto.
    + simpl.
      rewrite IHp.
      reflexivity.
    + simpl.
      rewrite IHp1.
      rewrite IHp2.
      reflexivity.
  Defined.

  Lemma Permutation_contains_refl: forall {l a} (Ct: Contains a l),
      Permutation_contains (Permutation_refl l) Ct = Ct.
  Proof.
    intros.
    induction Ct; auto.
    simpl.
    unfold eq_rect_r.
    unfold eq_rect.
    unfold eq_sym.
    rewrite IHCt.
    reflexivity.
  Defined.

  Lemma Permutation_contains_trans: forall {l1 l2 l3 a} (Ct: Contains a l1) (p1: l1 >< l2) (p2: l2 >< l3),
      Permutation_contains (perm_trans p1 p2) Ct
      = Permutation_contains p2 (Permutation_contains p1 Ct).
  Proof.
    intros.
    induction Ct; reflexivity.
  Defined.

  Lemma Contains_app_head_nil: forall {l x} (Ct: Contains x l),
      Contains_app_head [] Ct = Permutation_contains (Permutation_app_nil _) Ct.
  Proof.
    intros.
    dependent induction Ct; auto.
    simpl.
    rewrite IHCt.
    reflexivity.
  Defined.
End Permutation.

Notation "A1 >< A2" := (Permutation A1 A2) (at level 70).

(** ** Extensional equality between permutations *)

Section Permutation_eq.

  Context {A: Type}.

  Definition Permutation_eq {l1 l2: list A} (p1 p2: l1 >< l2) := forall (x: A) (Ct: Contains x l1),
      Permutation_contains p1 Ct = Permutation_contains p2 Ct.

  (** *** Lemmas about [Permutation_eq] *)
  Lemma Permutation_eq_refl: forall {l l'} (p: l >< l'), Permutation_eq p p.
  Proof.
    unfold Permutation_eq.
    reflexivity.
  Defined.

  Lemma Permutation_eq_sym: forall {l l'} (p p': l >< l'), Permutation_eq p p' -> Permutation_eq p' p.
  Proof.
    unfold Permutation_eq.
    intros.
    symmetry.
    apply H.
  Defined.

  Lemma Permutation_eq_trans: forall {l l'} (p p' p'': l >< l'),
      Permutation_eq p p' -> Permutation_eq p' p'' -> Permutation_eq p p''.
  Proof.
    unfold Permutation_eq.
    intros.
    rewrite H.
    apply H0.
  Defined.

  Lemma Permutation_eq_singleton: forall {a: A} (p: [a] >< [a]), Permutation_eq p (Permutation_refl _).
  Proof.
    intros.
    unfold Permutation_eq.
    intros.
    dependent destruction Ct.
    + simpl.
      unfold eq_rect_r.
      unfold eq_rect.
      unfold eq_sym.
      dependent induction p.
      * reflexivity.
      * apply Permutation_length_1_inv in p1 as eq; subst.
        rewrite Permutation_contains_trans.
        rewrite IHp1; auto.
    + dependent destruction Ct.
  Defined.

End Permutation_eq.
