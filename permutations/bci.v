(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

Reserved Infix "@" (at level 20, left associativity).
Record BCI :=
  {Carrier_set: Type;
   App: Carrier_set -> Carrier_set -> Carrier_set where "A @ B" := (App A B) ;
   B_combinator: Carrier_set;
   C_combinator: Carrier_set;
   I_combinator: Carrier_set;
   B_ax: forall x y z: Carrier_set, B_combinator @ x @ y @ z = x @ (y @ z);
   C_ax: forall x y z: Carrier_set, C_combinator @ x @ y @ z = x @ z @ y;
   I_ax: forall x: Carrier_set, I_combinator @ x = x;
  }.

Parameter bci: BCI.

Infix "@" := (App bci).
Notation "'Carrier'" := (Carrier_set bci).
Notation "'B'" := (B_combinator bci).
Notation "'C'" := (C_combinator bci).
Notation "'I'" := (I_combinator bci).

Ltac rewrite_BCI := repeat (rewrite (C_ax bci) || rewrite (B_ax bci) || rewrite (I_ax bci)).
