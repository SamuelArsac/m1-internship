(*
Copyright 2021 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Expressions with free variables and λ* *)
(** In order to implement λ*, this files provides a type for expressions containing free variables,
 as well as the implementation of λ* itself and the proof of its correctness. *)

Require Import List.
Import ListNotations.
Require Import RelationClasses.
Require Import Program.
Require Import Logic.FunctionalExtensionality.
Require Import bci.
Require Import permutation.

(** ** Expressions *)

Parameter Ty: Type.

Definition Env := list Ty.

Inductive Exp: Env -> Type :=
  | CAR: Carrier -> Exp []
  | VAR: forall (v: Ty), Exp [v]
  | APP: forall E E_fun E_arg, Exp E_fun -> Exp E_arg -> E >< E_fun ++ E_arg -> Exp E.

(** ** Functions for the [APP] case *)

Inductive APP_split: Ty -> Env -> Env -> Env -> Env -> Type :=
  | head_split: forall {x: Ty} {E1 E2 E_fun E_arg E_fun1 E_fun2: Env},
      E_fun >< E_fun1 ++ x :: E_fun2
      -> E_fun1 ++ E_fun2 ++ E_arg >< E1 ++ E2
      -> APP_split x E1 E2 E_fun E_arg
  | tail_split: forall {x: Ty} {E1 E2 E_fun E_arg E_arg1 E_arg2: Env},
      E_arg >< E_arg1 ++ x :: E_arg2
      -> E_fun ++ E_arg1 ++ E_arg2 >< E1 ++ E2
      -> APP_split x E1 E2 E_fun E_arg.

(** Some lemmas about the [APP_split] type *)

Lemma APP_split_skip: forall {x: Ty} {E1 E2 E_fun E_arg: Env},
    APP_split x E1 E2 E_fun E_arg -> forall y, APP_split x (y :: E1) E2 (y :: E_fun) E_arg.
  intros x E1 E2 E_fun E_arg APP_split_result y.
  inversion APP_split_result as [? ? ? ? ? E_fun1 E_fun2 p_fun p|
                                 ? ? ? ? ? E_arg1 E_arg2 p_arg p]; subst.
  + apply (head_split (E_fun1 := y :: E_fun1) (E1 := y :: E1)
                      (perm_skip y p_fun) (perm_skip y p)).
  + apply (tail_split (E_fun := y :: E_fun) (E1 := y :: E1)
                      p_arg (perm_skip y p)).
Defined.

Lemma APP_split_eq: forall {x: Ty} {E1 E2 E_fun E_arg: Env}, (E_fun ++ E_arg = E1 ++ x :: E2) ->
                                                        APP_split x E1 E2 E_fun E_arg.
Proof.
  intros x E1 E2.
  induction E1 as [|y]; intro E_fun; induction E_fun as [|z]; intros E_arg eq; simpl in eq.
  + apply (tail_split (E_arg1 := []) (E_arg2 := E2)); auto using Permutation_refl.
    rewrite eq.
    apply Permutation_refl.
  + inversion eq; subst.
    apply (head_split (E_fun1 := []) (E_fun2 := E_fun)); auto using Permutation_refl.
  + apply (tail_split (E_arg1 := (y :: E1)) (E_arg2 := E2)); auto using Permutation_refl.
    rewrite eq.
    apply Permutation_refl.
  + inversion eq as [[eq1 eq']]; subst.
    apply IHE1 in eq'.
    apply APP_split_skip.
    assumption.
Defined.

Lemma APP_split_swap: forall (x: Ty) (E1 E2 E_fun E_arg: Env),
    APP_split x E1 E2 E_fun E_arg -> forall y z, APP_split x (y :: z :: E1) E2 (z :: y :: E_fun) E_arg.
  intros x E1 E2 E_fun E_arg APP_split_result y z.
  inversion APP_split_result as [? ? ? ? ? E_fun1 E_fun2 p1 p2|
                                 ? ? ? ? ? E_arg1 E_arg2 p1 p2];
    subst.
  + apply (head_split (E_fun1 := z :: y :: E_fun1) (E1 := y :: z :: E1) (perm_skip z (perm_skip y p1))
                      (perm_trans (perm_skip z (perm_skip y p2)) (perm_swap y z (E1 ++ E2)))).
  + apply (tail_split (E_fun := z :: y :: E_fun) (E1 := y :: z :: E1) p1
                      (perm_trans (perm_skip z (perm_skip y p2)) (perm_swap y z (E1 ++ E2)))).
Defined.

(** The function used to obtain an object of type [APP_split] *)

Definition APP_splitter: forall (x: Ty) (E1 E2 E_fun E_arg: Env), E_fun ++ E_arg >< E1 ++ x :: E2 ->
                                                             APP_split x E1 E2 E_fun E_arg.
  intros x E1 E2 E_fun E_arg p.
  remember (E1 ++ x :: E2) as l in p.
  remember (E_fun ++ E_arg) as l' in p.
  apply symmetry in Heql'.
  revert x E1 E2 E_fun E_arg Heql Heql'.
  induction p as [
                |x l' l p
                |x y l
                |l1 l2 l3 p1 IHp1 p2 IHp2]; intros z E1 E2 E_fun E_arg Heql Heql'.
  + apply app_cons_not_nil in Heql.
    contradiction.
  + destruct E1 as [|x']; inversion Heql as [[Heql1 Heql2]]; subst.
    * destruct E_fun as [|z']; inversion Heql' as [[Heql'1 Heql'2]]; subst.
      ++ apply (tail_split (E_arg1 := []) (E_arg2 := l')); auto.
         rewrite<- Heql'1.
         apply Permutation_refl.
      ++ apply (head_split (E_fun1 := []) (E_fun2 := E_fun)); auto using Permutation_refl.
    * destruct E_fun as [|x]; inversion Heql' as [[Heql'1 Heql'2]]; subst.
      ++ pose (exists_eq := Permutation_vs_elt_inv p).
         destruct exists_eq as [l'1 [l'2 eq]].
         rewrite eq in p.
         apply (tail_split (E_arg1 := x' :: E1) (E_arg2 := E2)); auto using Permutation_refl.
         rewrite eq in Heql'.
         simpl in Heql'.
         rewrite Heql'.
         apply perm_skip.
         assumption.
      ++ destruct (IHp z E1 E2 E_fun E_arg) as [z E1 E2 E_fun E_arg E_fun1 E_fun2 p1 p2
                                               |z E1 E2 E_fun E_arg E_arg1 E_arg2 p1 p2]; auto.
         ** apply (head_split (E_fun1 := x' :: E_fun1) (E1 := x' :: E1)
                              (perm_skip x' p1) (perm_skip x' p2)).
         ** apply (tail_split (E_fun := x' :: E_fun) (E1 := x' :: E1) p1 (perm_skip x' p2)).
  + destruct E_fun as [|y']; subst.
    * destruct E1 as [|x']; inversion Heql as [[Heql1 Heql2]]; subst.
      ++ apply (tail_split (E_arg1 := [y]) (E_arg2 := l)); auto using Permutation_refl.
         simpl in Heql'.
         rewrite Heql'.
         apply Permutation_refl.
      ++ destruct E1 as [|y']; inversion Heql2; subst.
         ** eapply (tail_split (E_arg1 := []) (E_arg2 := x' :: E2)); auto using Permutation_refl.
            rewrite<- Heql'.
            apply Permutation_refl.
         ** apply (tail_split (E_arg1 := x' :: y' :: E1) (E_arg2 := E2)); auto using Permutation_refl.
            pose (p := perm_swap x' y' (E1 ++ z :: E2)).
            rewrite<- Heql' in p.
            assumption.
    * destruct E_fun as [|x']; inversion Heql' as [[Heql'1 Heql'2]]; subst.
      ++ destruct E1 as [|x']; inversion Heql as [[Heql1 Heql2]]; subst.
         ** apply (tail_split (E_arg1 := []) (E_arg2 := l)); auto using Permutation_refl.
         ** destruct E1 as [|y']; inversion Heql2 as [[Heql21 Heql22]]; subst.
            +++ apply (head_split (E_fun1 := []) (E_fun2 := [])); auto using Permutation_refl.
            +++ apply (tail_split (E_arg1 := x' :: E1) (E_arg2 := E2)
                                  (Permutation_refl (x' :: E1 ++ z :: E2))).
                apply perm_swap.
      ++ destruct E1 as [|x']; inversion Heql as [[Heql1 Heql2]]; subst.
         ** apply (head_split (E_fun1 := [y]) (E_fun2 := E_fun)); auto using Permutation_refl.
         ** destruct E1 as [|y']; inversion Heql2 as [[Heql21 Heql22]]; subst.
            +++ apply (head_split (E_fun1 := []) (E_fun2 := x' :: E_fun)); auto using Permutation_refl.
            +++ apply APP_split_swap.
                apply APP_split_eq.
                assumption.
  + subst.
    apply Permutation_vs_elt_inv in p2.
    destruct p2 as [l21 [l22 eq1]].
    assert (APP_split_res1: APP_split z l21 l22 E_fun E_arg); auto.
    assert (APP_split_res2: APP_split z E1 E2 [] (l21 ++ z :: l22)); auto.
    remember (l21 ++ z :: l22) as l in APP_split_res2.
    remember [] as empty in APP_split_res2.
    generalize Heqempty.
    generalize Heql.
    clear Heql.
    clear Heqempty.
    destruct APP_split_res2 as [z E1 E2 ? ? E_fun1 E_fun2 p2 p3
                               |z E1 E2 ? ? E_arg1 E_arg2 p2 p3];
      intros eq2 E_fun0_nil; subst.
    * apply Permutation_nil in p2.
      destruct E_fun1; inversion p2.
    * destruct APP_split_res1 as [z l21 l22 ? ? E_fun1' E_fun2' p4 p5
                                 |z l21 l22 ? ? E_arg1' E_arg2' p4 p5]; subst;
      pose (p6 := perm_trans (perm_trans p5 (Permutation_app_inv p2)) p3).
      ++ apply (head_split p4 p6).
      ++ apply (tail_split p4 p6).
Defined.

(** A lemma for reasoning about the [APP_splitter] output in the correctness proof *)

Lemma APP_splitter_destruct: forall (x: Ty) E1 E2 E_fun E_arg p,
    {E_fun1 & {E_fun2 & {p1 & {p' | APP_splitter x E1 E2 E_fun E_arg p =
                                    @head_split x E1 E2 E_fun E_arg E_fun1 E_fun2 p1 p'}}}}
    +
    {E_arg1 & {E_arg2 & {p2 & {p' | APP_splitter x E1 E2 E_fun E_arg p =
                                    @tail_split x E1 E2 E_fun E_arg E_arg1 E_arg2 p2 p'}}}}.
Proof.
  intros.
  destruct (APP_splitter x E1 E2 E_fun E_arg p) as [x E1 E2 E_fun E_arg E_fun1 E_fun2 p1 p'
                                                   |x E1 E2 E_fun E_arg E_arg1 E_arg2 p2 p'].
  + left.
    exists E_fun1.
    exists E_fun2.
    exists p1.
    exists p'.
    reflexivity.
  + right.
    exists E_arg1.
    exists E_arg2.
    exists p2.
    exists p'.
    reflexivity.
Defined.

(** ** Implementing λ* *)
(** The [lambda_s] function, implementing λ* for expressions *)

Definition lambda_s (v: Ty) E (e: Exp E): forall E1 E2, E >< E1 ++ v :: E2 -> Exp (E1 ++ E2).
  induction e as [ |v'|E E_fun E_arg e_fun IHe_fun e_arg IHe_arg p1].
  + intros E1 E2 p.
    apply Permutation_nil in p.
    destruct E1; inversion p.
  + intros E1 E2 p2.
    apply Permutation_length_1_inv in p2.
    destruct E1; inversion p2; subst.
    * apply (CAR I).
    * destruct E1; inversion H1.
  + intros E1 E2 p2.
    apply Permutation_sym in p1.
    pose (p12 := perm_trans p1 p2).
    destruct (APP_splitter v E1 E2 E_fun E_arg p12) as [x E1 E2 E_fun E_arg E_fun1 E_fun2 p3 p4
                                                      |x E1 E2 E_fun E_arg E_arg1 E_arg2 p3 p4].
    * eapply APP.
      ++ eapply APP.
         ** apply (CAR C).
         ** apply IHe_fun.
            apply p3.
         ** apply Permutation_refl.
      ++ apply e_arg.
      ++ apply Permutation_sym.
         simpl.
         rewrite<- app_assoc_def.
         assumption.
    * eapply APP.
      ++ eapply APP.
         ** apply (CAR B).
         ** apply e_fun.
         ** apply Permutation_refl.
      ++ apply IHe_arg.
         apply p3.
      ++ apply Permutation_sym.
         assumption.
Defined.

(** ** Environments for free variables *)

Definition Bci_env E := forall v: Ty, Contains v E -> Carrier.

(** Two examples of environments *)

Definition empty_env: Bci_env [].
  unfold Bci_env.
  intros v Ct.
  inversion Ct.
Defined.

Program Definition singleton_env BE v: Bci_env [v] :=
  fun v' Ct =>
    match Ct with
      | Here _ _ => BE
      | Next _ _ _ _ => _
    end.

(** *** Lemmas for manipulating environments *)

Lemma Split_bci_env1: forall E1 E2 (BE: Bci_env (E1 ++ E2)), Bci_env E1.
Proof.
  intros E1 E2 BE v Ct.
  apply (BE v).
  apply Contains_app_head.
  assumption.
Defined.

Lemma Split_bci_env2: forall E1 E2 (BE: Bci_env (E1 ++ E2)), Bci_env E2.
Proof.
  intros E1 E2 BE v Ct.
  apply (BE v).
  apply Contains_app_tail.
  assumption.
Defined.

Lemma Bci_env_convert: forall E1 E2 (p: E2 >< E1), Bci_env E1 -> Bci_env E2.
Proof.
  intros E1 E2 p BE v Ct.
  apply (BE v).
  apply (Permutation_contains p Ct).
Defined.

Lemma Split_bci_env_refl1: forall E (BE: Bci_env (E ++ [])),
    Split_bci_env1 E [] BE = fun v (Ct: Contains v E) =>
                               BE v (Permutation_contains
                                       (Permutation_app_nil E) Ct).
Proof.
  intros.
  extensionality v.
  extensionality Ct.
  dependent induction Ct.
  + reflexivity.
  + apply (IHCt (fun v Ct => BE v (Next v (l ++ []) a' Ct))).
Qed.

Lemma Split_bci_env_refl2: forall E (BE: Bci_env ([] ++ E)), Split_bci_env2 [] E BE = BE.
Proof.
  intros.
  extensionality v.
  extensionality Ct.
  dependent induction Ct.
  + reflexivity.
  + apply (IHCt (fun v Ct => BE v (Next v l a' Ct))).
Qed.

Lemma Bci_env_insert: forall (E: Env) (BE: Bci_env E) (x: Ty) (c: Carrier), Bci_env (x :: E).
Proof.
  intros E BE x c v Ct.
  remember (x :: E) as l.
  revert Heql.
  revert BE.
  revert E.
  revert x.
  destruct Ct; intros.
  + apply c.
  + inversion Heql; subst.
    apply (BE v).
    assumption.
Defined.

Lemma Bci_env_insert_mid: forall (E1 E2: Env) (BE: Bci_env (E1 ++ E2)) (x: Ty) (c: Carrier),
    Bci_env (E1 ++ x :: E2).
Proof.
  intros.
  pose (BE' := Bci_env_insert _ BE x c).
  pose (p := Permutation_sym (Permutation_app_tail E2 (Permutation_cons_append E1 x))).
  simpl in p.
  rewrite<- app_assoc_def in p.
  apply (Bci_env_convert _ _ p BE').
Defined.

Lemma Bci_convert_trans: forall E1 E2 E3 (p1: E3 >< E2) (p2: E2 >< E1) (BE: Bci_env E1),
    Bci_env_convert E2 E3 p1 (Bci_env_convert E1 E2 p2 BE) =
    Bci_env_convert E1 E3 (perm_trans p1 p2) BE.
Proof.
  intros.
  induction p1; auto.
Defined.

Lemma Bci_insert_nil: forall (BE: Bci_env []) (c: Carrier) (v: Ty) Ct,
    Bci_env_insert [] BE _ c v Ct = singleton_env c v v Ct.
Proof.
  intros.
  dependent destruction Ct; auto.
  inversion Ct.
Defined.

Lemma Bci_convert_singleton: forall (v: Ty) (p: [v] >< [v]) (BE: Bci_env [v]),
    Bci_env_convert [v] [v] p BE = BE.
Proof.
  intros.
  extensionality v'.
  extensionality Ct.
  revert Ct.
  dependent destruction Ct.
  + simpl.
    dependent induction p; intros.
    * dependent destruction p; auto.
    * assert (eq := (Permutation_length_1_inv p1)).
      subst.
      rewrite<- Bci_convert_trans.
      assert (IHp2_ext: forall BE' : Bci_env [v], Bci_env_convert [v] [v] p2 BE' = BE').
      ++ intro.
         extensionality v'.
         extensionality Ct.
         dependent destruction Ct.
         ** apply IHp2; auto.
         ** inversion Ct.
      ++ rewrite IHp2_ext.
         apply IHp1; auto.
  + inversion Ct.
Defined.

Lemma Bci_env_refl: forall E1 E2, E2 = E1 -> forall (BE: Bci_env E1), Bci_env E2.
  intros E1 E2 eq BE.
  rewrite eq.
  assumption.
Defined.

Lemma Bci_env_convert_refl: forall E, Bci_env_convert E E (Permutation_refl E) =
                                 (fun BE => BE).
Proof.
  intro.
  extensionality BE.
  extensionality v.
  extensionality Ct.
  unfold Bci_env_convert.
  rewrite Permutation_contains_refl.
  reflexivity.
Qed.

(** *** The main function, turning an expression into a [Carrier] object *)

Fixpoint Exp_to_BCI {E} (e: Exp E): Bci_env E -> Carrier :=
  match e with
    | CAR c => fun BE => c
    | VAR v => fun BE => BE v (Here v _)
    | APP _ E1 E2 e1 e2 p => fun BE =>
                              Exp_to_BCI e1 (Split_bci_env1 _ _
                                                            (Bci_env_convert _ _
                                                                             (Permutation_sym p)
                                                                             BE))
                                         @ Exp_to_BCI e2
                                         (Split_bci_env2 _ _
                                                         (Bci_env_convert _ _
                                                                          (Permutation_sym p)
                                                                          BE))
  end.

(** ** Correctness of [lambda_s] *)

Theorem lambda_s_correctness: forall E (e: Exp E) E1 E2 (v: Ty) (p: E >< E1 ++ v :: E2)
                                (x: Bci_env (E1 ++ E2)) (c: Carrier),
    exists p', Permutation_eq p p' /\
          Exp_to_BCI e (Bci_env_convert _ _ p' (Bci_env_insert_mid E1 E2 x v c))
          = Exp_to_BCI (lambda_s v E e E1 E2 p') x @ c.
Proof.
  intros E e.
  induction e; intros;
    (* Contradiction for B, C and I *)
    try (apply Permutation_nil in p as eq;
         destruct E1; inversion eq).
  + apply Permutation_length_1_inv in p as eq.
    destruct E1; inversion eq; subst.
    * exists (Permutation_refl [v]).
      split.
      ++ apply Permutation_eq_singleton.
      ++ simpl.
         rewrite I_ax.
         reflexivity.
    * destruct E1; inversion H1.
  + pose (p0' := p0).
    exists p0'.
    split.
    * apply Permutation_eq_refl.
    * simpl.
      destruct (APP_splitter_destruct v E1 E2 E_fun E_arg
                                      (perm_trans (Permutation_sym p) p0')).
      ++ destruct s as [E_fun1 [E_fun2 [p1 [p' H]]]].
         rewrite H.
         simpl.
         rewrite C_ax.
         rewrite Split_bci_env_refl2.
         rewrite Permutation_sym_refl.
         rewrite Bci_env_convert_refl.
         rewrite Permutation_sym_double.
         apply f_equal2.
         ** admit.
         ** admit.
      ++ destruct s as [E_arg1 [E_arg2 [p2 [p' H]]]].
         rewrite H.
         simpl.
         rewrite B_ax.
         rewrite Split_bci_env_refl2.
         rewrite Permutation_sym_refl.
         rewrite Permutation_sym_double.
         rewrite Bci_env_convert_refl.
         apply f_equal2.
         ** admit.
         ** admit.
Admitted.

(** ** Examples of encodings *)

Parameter P Q x y z D: Ty.

(** ** Pairs *)
(** Pair := lam* x y z. z x y *)

Definition Pair_exp: Exp ([] ++ []).
  eapply (lambda_s x).
  + eapply (lambda_s y) with (E1 := [x]) (E2 := []).
    * eapply (lambda_s z) with (E1 := []) (E2 := [x; y]).
      ++ eapply APP.
         ** eapply APP.
            +++ apply (VAR z).
            +++ apply (VAR x).
            +++ apply Permutation_refl.
         ** apply (VAR y).
         ** apply Permutation_refl.
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** ** Booleans *)

(** T_bool := lam* x y. Pair @ x @ y *)

Definition T_bool_exp: Exp ([] ++ []).
  eapply (lambda_s x).
  + eapply (lambda_s y) with (E1 := [x]) (E2 := []).
    * eapply APP.
      ++ eapply APP.
         ** apply Pair_exp.
         ** apply (VAR x).
         ** apply Permutation_refl.
      ++ apply (VAR y).
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** F_bool := lam* x y. Pair y x *)

Definition F_bool_exp: Exp ([] ++ []).
  eapply (lambda_s x).
  + eapply (lambda_s y) with (E1 := []) (E2 := [x]).
    * eapply APP.
      ++ eapply APP.
         ** apply Pair_exp.
         ** apply (VAR y).
         ** apply Permutation_refl.
      ++ apply (VAR x).
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** *** Boolean operations *)

(** Not := lam* P x y. P y x *)

Definition Not_exp: Exp ([] ++ []).
  eapply (lambda_s P).
  + eapply (lambda_s x) with (E1 := [P]) (E2 := []).
    * eapply (lambda_s y) with (E1 := [P]) (E2 := [x]).
      ++ eapply APP.
         ** eapply APP.
            +++ apply (VAR P).
            +++ apply (VAR y).
            +++ apply Permutation_refl.
         ** apply (VAR x).
         ** apply Permutation_refl.
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** Elim := lam* x. x @ I @ I @ I *)

Definition Elim_exp: Exp ([] ++ []).
  eapply (lambda_s x).
  + eapply APP.
    * eapply APP.
      ++ eapply APP.
         ** apply (VAR x).
         ** apply (CAR I).
         ** apply Permutation_refl.
      ++ apply (CAR I).
      ++ apply Permutation_refl.
    * apply (CAR I).
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** Or := lam* P Q. P @ T_bool @ Q @ (lam* x y. id @ y @ x) *)

Definition Or_exp: Exp ([] ++ []).
  eapply (lambda_s P).
  + eapply (lambda_s Q) with (E1 := [P]) (E2 := []).
    * eapply APP.
      ++ eapply APP.
         ** eapply APP.
            +++ apply (VAR P).
            +++ apply T_bool_exp.
            +++ apply Permutation_refl.
         ** apply (VAR Q).
         ** apply Permutation_refl.
      ++ eapply (lambda_s x).
         ** eapply (lambda_s y) with (E1 := []) (E2 := [x]).
            +++ eapply APP.
                *** eapply APP.
                    ++++ apply Elim_exp.
                    ++++ apply (VAR y).
                    ++++ apply Permutation_refl.
                *** apply (VAR x).
                *** apply Permutation_refl.
            +++ apply Permutation_refl.
         ** apply Permutation_refl.
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** And := lam* P Q. P @ F_bool @ Q @ Elim *)

Definition And_exp: Exp ([] ++ []).
  eapply (lambda_s P).
  + eapply (lambda_s Q) with (E1 := [P]) (E2 := []).
    * eapply APP.
      ++ eapply APP.
         ** eapply APP.
            +++ apply (VAR P).
            +++ apply F_bool_exp.
            +++ apply Permutation_refl.
         ** apply (VAR Q).
         ** apply Permutation_refl.
      ++ apply Elim_exp.
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** *** If-then-else *)

(** ITE := lam* P x y D. P @ y @ x @ D
 with D @ x = I and D @ y = I *)

Definition ITE_exp: Exp ([] ++ []).
  eapply (lambda_s P).
  + eapply (lambda_s x) with (E1 := [P]) (E2 := []).
    * eapply (lambda_s y) with (E1 := [P]) (E2 := [x]).
      ++ eapply (lambda_s D) with (E1 := [P; y; x]) (E2 := []).
         ** eapply APP.
            +++ eapply APP.
                *** eapply APP.
                    ++++ apply (VAR P).
                    ++++ apply (VAR y).
                    ++++ apply Permutation_refl.
                *** apply (VAR x).
                *** apply Permutation_refl.
            +++ apply (VAR D).
            +++ apply Permutation_refl.
         ** apply Permutation_refl.
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** *** Duplication for booleans *)

Parameter U V u1 u2 v1 v2: Ty.

(** Copy := lam* P. P @ (Pair @ T_bool @ T_bool) @ (Pair @ F_bool @ F_bool)
                     @ (lam* U V. U @ (lam* u1 u2.
                                        V @ (lam* v1 v2.
                                              (Pair @ (id @ v1 @ u1) @ (id @ v2 @ u2))))) *)

Definition Copy_exp: Exp ([] ++ []).
  eapply (lambda_s P).
  + eapply APP.
    * eapply APP.
      ++ eapply APP.
         ** apply (VAR P).
         ** eapply APP.
            +++ eapply APP.
                *** apply Pair_exp.
                *** apply T_bool_exp.
                *** apply perm_nil.
            +++ apply T_bool_exp.
            +++ apply perm_nil.
         ** apply Permutation_refl.
      ++ eapply APP.
         ** eapply APP.
            +++ apply Pair_exp.
            +++ apply F_bool_exp.
            +++ apply perm_nil.
         ** apply F_bool_exp.
         ** apply perm_nil.
      ++ apply Permutation_refl.
    * eapply (lambda_s U) with (E1 := []) (E2 := []).
      ++ eapply (lambda_s V) with (E1 := [U]) (E2 := []).
         ** eapply APP.
            +++ apply (VAR U).
            +++ eapply (lambda_s u1) with (E1 := [V]) (E2 := []).
                *** eapply (lambda_s u2) with (E1 := [V; u1]) (E2 := []).
                    ++++ eapply APP.
                         **** apply (VAR V).
                         **** eapply (lambda_s v1) with (E1 := []) (E2 := [u1; u2]).
                              +++++ eapply (lambda_s v2) with (E1 := [v1; u1]) (E2 := [u2]).
                              ***** eapply APP.
                              ++++++ eapply APP.
                              ****** apply Pair_exp.
                              ****** eapply APP.
                              +++++++ eapply APP.
                              ******* apply Elim_exp.
                              ******* apply (VAR v1).
                              ******* apply Permutation_refl.
                              +++++++ apply (VAR u1).
                              +++++++ apply Permutation_refl.
                              ****** apply Permutation_refl.
                              ++++++ eapply APP.
                              ****** eapply APP.
                              +++++++ apply Elim_exp.
                              +++++++ apply (VAR v2).
                              +++++++ apply Permutation_refl.
                              ****** apply (VAR u2).
                              ****** apply Permutation_refl.
                              ++++++ apply Permutation_refl.
                              ***** apply Permutation_refl.
                              +++++ apply Permutation_refl.
                         **** apply Permutation_refl.
                    ++++ apply Permutation_refl.
                *** apply Permutation_refl.
            +++ apply Permutation_refl.
         ** apply Permutation_refl.
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** *** Elimintor for pairs *)
(** lam* P. P @ (lam* x y. (x_elim @ x) @ (y_elim @ y)) *)

Definition Pair_elim (x_elim y_elim: Exp ([])): Exp([] ++ []).
  eapply (lambda_s P).
  + eapply APP.
    * apply (VAR P).
    * eapply (lambda_s x) with (E1 := []) (E2 := []).
      ++ eapply (lambda_s y).
         ** eapply APP.
            +++ eapply APP.
                *** apply x_elim.
                *** apply (VAR x).
                *** apply Permutation_refl.
            +++ eapply APP.
                *** apply y_elim.
                *** apply (VAR y).
                *** apply Permutation_refl.
            +++ apply Permutation_refl.
         ** apply Permutation_refl.
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.

(** *** Duplication for pairs *)
(** lam* P. P @ (lam* x y. (x_copy @ x) @
(lam* x1 x2. (y_copy @ y) @ (lam* y1 y2. Pair @ (Pair @ x1 @ y1) @ (Pair @ x2 @ y2)))) *)

Parameter x1 x2 y1 y2: Ty.

Definition Pair_copy (x_copy y_copy: Exp []): Exp ([] ++ []).
  eapply (lambda_s P).
  + eapply APP.
    * apply (VAR P).
    * eapply (lambda_s x) with (E1 := []) (E2 := []).
      ++ eapply (lambda_s y) with (E1 := [x]) (E2 := []).
         ** eapply APP.
            +++ eapply APP.
                *** apply x_copy.
                *** apply (VAR x).
                *** apply Permutation_refl.
            +++ eapply (lambda_s x1) with (E1 := [y]) (E2 := []).
                *** eapply (lambda_s x2) with (E1 := [y; x1]) (E2 := []).
                    ++++ eapply APP.
                         **** eapply APP.
                              +++++ apply y_copy.
                              +++++ apply (VAR y).
                              +++++ apply Permutation_refl.
                         **** eapply (lambda_s y1) with (E1 := [x1]) (E2 := [x2]).
                              +++++ eapply (lambda_s y2) with (E1 := [x1; y1; x2]) (E2 := []).
                              ***** eapply APP.
                              ++++++ eapply APP.
                              ****** apply Pair_exp.
                              ****** eapply APP.
                              +++++++ eapply APP.
                              ******* apply Pair_exp.
                              ******* apply (VAR x1).
                              ******* apply Permutation_refl.
                              +++++++ apply (VAR y1).
                              +++++++ apply Permutation_refl.
                              ****** apply Permutation_refl.
                              ++++++ eapply APP.
                              ****** eapply APP.
                              +++++++ apply Pair_exp.
                              +++++++ apply (VAR x2).
                              +++++++ apply Permutation_refl.
                              ****** apply (VAR y2).
                              ****** apply Permutation_refl.
                              ++++++ apply Permutation_refl.
                              ***** apply Permutation_refl.
                              +++++ apply Permutation_refl.
                         **** apply Permutation_refl.
                    ++++ apply Permutation_refl.
                *** apply Permutation_refl.
            +++ apply Permutation_refl.
         ** apply Permutation_refl.
      ++ apply Permutation_refl.
    * apply Permutation_refl.
  + apply Permutation_refl.
Defined.
